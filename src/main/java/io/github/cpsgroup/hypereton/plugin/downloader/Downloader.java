package io.github.cpsgroup.hypereton.plugin.downloader;

import io.github.cpsgroup.hypereton.plugin.Plugin;

import java.util.List;

/**
 * Interface for downloader plugins.
 * Implementing classes must be annotated with org.springframework.stereotype.Component in order for plugin loading to work.
 * <p/>
 * Created by Manuel Weidmann on 06.09.2014.
 */
public interface Downloader extends Plugin {

    /**
     * Login, Download of Documents and logout from Makrolog (recht.makrolog.de)
     *
     * @param normIds is a list of normID's of Makrolog Documents
     * @return true if all individual downloads were successful
     */
    public List<Boolean> download(List<String> normIds);

    /**
     * Download a single document from Makrolog
     *
     * @param normId
     * @return true if the document has been downloaded successfully
     */
    public boolean download(String normId);

}
